from os import getenv
import json
import requests
import pymssql
import ast
import mysql.connector

mydb = mysql.connector.connect(
host = 'rds-mysql-mentalhelp.cfgrep4u2t84.us-east-1.rds.amazonaws.com',
user = 'mentalhelp',
passwd = 'qwerqwer',
database = 'mentalhelp')
mycursor = mydb.cursor()

illnesses = ["ADHD", "Schizophrenia", "Anxiety Disorder", "Bipolar Disorder", "Borderline Personality Disorder", 
"Post-Traumatic Stress Disorder", "Dissociative Identity Disorder", "Obsessive-Compulsive Disorder", "Narcolepsy", "Depression"]

for x in illnesses:
	parameters = {'api_key':'vrfmjigrnphqiacj', 'CategoryID':'18', 'keyword':x}
	firstUrl = "https://healthfinder.gov/FreeContent/Developer/Search.xml"
	html = requests.get(firstUrl, params = parameters)
	info = json.loads(html.text.encode("utf-8"))
	sql = "INSERT INTO Illnesses VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s)"
	val = (info["name"], info["population"], info["average_age"], info["genetic"], 
		info["chronic"], info["symptoms"], info["treatments"], info["curable"])
	mycursor.execute(sql, val)
	mydb.commit()

mycursor.close()
mydb.close()
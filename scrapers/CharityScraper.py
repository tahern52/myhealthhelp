from os import getenv
import json
import requests
import pymssql
import ast
import mysql.connector

ids = [363379124, 134131139, 953371166]

for x in ids:
	parameters = {'app_id':'04ac6a77', 'app_key':'0c8605c377a7c2983fdfe7db4962619e'}
	firstUrl = "https://api.data.charitynavigator.org/v2/Organizations/"+str(x)
	html = requests.get(firstUrl, params = parameters)
	info = json.loads(html.text.encode("utf-8"))

	mydb = mysql.connector.connect(
	host = 'rds-mysql-mentalhelp.cfgrep4u2t84.us-east-1.rds.amazonaws.com',
	user = 'mentalhelp',
	passwd = 'qwerqwer',
	database = 'mentalhelp')
	mycursor = mydb.cursor()

	sql = "INSERT INTO Charities VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
	val = (info["charityName"], info["mailingAddress"]["stateOrProvince"], info["tagLine"], 
		info["currentRating"]["score"], info["mission"], info["websiteURL"], 
		"www.google.com", info["irsClassification"]["deductibility"], 
		info["irsClassification"]["assetAmount"], 
		info["irsClassification"]["incomeAmount"])
	mycursor.execute(sql, val)
	mydb.commit()
	mycursor.close()
	mydb.close()
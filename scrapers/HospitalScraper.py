from os import getenv
import json
import requests
import pymssql
import ast
import mysql.connector

mydb = mysql.connector.connect(
host = 'rds-mysql-mentalhelp.cfgrep4u2t84.us-east-1.rds.amazonaws.com',
user = 'mentalhelp',
passwd = 'qwerqwer',
database = 'mentalhelp')
mycursor = mydb.cursor()

firstUrl = "https://services1.arcgis.com/Hp6G80Pky0om7QvQ/arcgis/rest/services/Hospitals/FeatureServer/0/query?where=TYPE%20%3D%20'PSYCHIATRIC'%20AND%20STATUS%20%3D%20'OPEN'&outFields=NAME,ADDRESS,CITY,STATE,ZIP,TELEPHONE,TYPE,POPULATION,COUNTY,COUNTYFIPS,LATITUDE,LONGITUDE,WEBSITE,OWNER,COUNTRY,STATUS&returnGeometry=false&outSR=4326&f=json"
html = requests.get(firstUrl)
info = json.loads(html.text.encode("utf-8"))["features"]
hospitals = {"AUSTIN STATE HOSPITAL", "HARRIS COUNTY PSYCHIATRIC CENTER", "CROSS CREEK HOSPITAL", "MCLEAN HOSPITAL CORPORATION", 
	"THE MENNINGER CLINIC", "SHEPPARD AND ENOCH PRATT HOSPITAL", "RESNICK NEUROPSYCHIATRIC HOSPITAL AT UCLA", "ROLLING HILLS HOSPITAL", "CUMBERLAND HALL HOSPITAL", "RIVER VALLEY BEHAVIORAL HEALTH"}
for x in info:
	if x["attributes"]["NAME"] in hospitals:
		sql = "INSERT INTO Hospitals VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
		a = x["attributes"]
		val = (a["NAME"], a["OWNER"], a["POPULATION"], 
			a["WEBSITE"], a["LATITUDE"], a["LONGITUDE"], 
			a["ADDRESS"], a["STATE"], 
			a["CITY"], a["ZIP"])
		mycursor.execute(sql, val)
		
mydb.commit()
mycursor.close()
mydb.close()
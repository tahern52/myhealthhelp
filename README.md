# Mental Health Help
### CS343 Software Engineering Project Backend
This repo contains the backend pipeline for our website at http://mentalhelp.me:
- API (http://api.mentalhelp.me)
- Database
- Data Scrapers
- Reports
- Backend Tests

## Team Member Stats
Taher Naeem
- GitLab ID: tahern52
- UT EID: tn7556
- Estimated time to completion: 20 hours
- Actual time to completion: 35 hours

Shreyas Tawre
- GitLab ID: stawre
- UT EID: sst693
- Estimated time to completion: 20 hours
- Actual time to completion: 50 hours

Weihan He
- GitLab ID: 847605543
- UT EID: wh7458
- Estimated time to completion: 20 hours
- Actual time to completion: 30 hours

Caleb Hamada
- GitLab ID: chamada1
- UT EID: ch37657
- Estimated time to completion: 25 hours
- Actual time to completion: 40 hours

Ho Yin (Jason) Cheng
- GitLab ID: Jason2713
- UT EID: hc27469
- Estimated time to completion: 10 hours
- Actual time to completion: 10 hours
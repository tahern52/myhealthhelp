FILES :=                                  \
	makefile                              \
	tests/test-api.py

all: run

TestAPI.pyx: tests/test-api.py
	coverage run --branch tests/test-api.py
	coverage report -m

run: TestAPI.pyx

"""
This file defines the database models 
"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy, BaseQuery
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_searchable import make_searchable, search, SearchQueryMixin
# from sqlalchemy_utils.types import TSVectorType
#from main import db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://mentalhelp:qwerqwer@rds-mysql-mentalhelp.cfgrep4u2t84.us-east-1.rds.amazonaws.com/mentalhelp"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 20
db = SQLAlchemy(app)

make_searchable(declarative_base().metadata)

# Query classes for SQLAlchemy syntax
class CharityQuery(BaseQuery, SearchQueryMixin):
    pass
class HospitalQuery(BaseQuery, SearchQueryMixin):
    pass
class IllnessQuery(BaseQuery, SearchQueryMixin):
    pass

class Charity (db.Model):
	"""
	Charity database model
	"""

	# Database columns
	id = db.Column(db.Integer, primary_key=True, unique=True) 
	name = db.Column(db.String(128), unique=True)
	state = db.Column(db.String(64))
	twitter = db.Column(db.String(128))
	tagLine = db.Column(db.String(128))
	rating = db.Column(db.Integer)
	mission = db.Column(db.Text)
	website_url = db.Column(db.String(128))
	image_url = db.Column(db.String(128))
	deductible = db.Column(db.String(64))
	assetAmount = db.Column(db.Integer)
	incomeAmount = db.Column(db.Integer)
	hospital_id = db.Column(db.Integer)
	illness_id = db.Column(db.Integer)

	# Query class and name of the table
	query_class = CharityQuery
	__tablename__ = 'Charities'

	# Representation String
	def __repr__(self):
		return '<Charity %s>' % self.name

class Illness (db.Model):
	"""
	Illness database model
	"""

	# Database columns
	id = db.Column(db.Integer, primary_key=True, unique=True) 
	name = db.Column(db.String(128), unique=True)
	image_url = db.Column(db.String(128))
	videoId = db.Column(db.String(128))
	population = db.Column(db.Integer)
	average_age = db.Column(db.Integer)
	genetic = db.Column(db.String(64))
	chronic = db.Column(db.String(64))
	symptoms = db.Column(db.String(128))
	treatments = db.Column(db.String(128))
	curable = db.Column(db.String(64))
	hospital_id = db.Column(db.Integer)
	charity_id = db.Column(db.Integer)

	# Query class and name of the table
	query_class = IllnessQuery
	__tablename__ = 'Illnesses'

	# Representation String
	def __repr__(self):
		return '<Illness %s>' % self.name

class Hospital (db.Model):
	"""
	Hospital database model
	"""

	# Database columns
	id = db.Column(db.Integer, primary_key=True, unique=True) 
	name = db.Column(db.String(128), unique=True)
	image_url = db.Column(db.String(128))
	owner = db.Column(db.String(64))
	population = db.Column(db.Integer)
	website_url = db.Column(db.String(128))
	latitude = db.Column(db.Float)
	longitude = db.Column(db.Float)
	address = db.Column(db.String(128))
	state = db.Column(db.String(64))
	city = db.Column(db.String(64))
	zip_code = db.Column(db.String(64))
	charity_id = db.Column(db.Integer)
	illness_id = db.Column(db.Integer)

	# Query class and name of the table
	query_class = HospitalQuery
	__tablename__ = 'Hospitals'

	# Representation String
	def __repr__(self):
		return '<Hospital %s>' % self.name

if __name__=='__main__':
    """
    Set up the database by configuring mappers used for search,
    creating tables based on the model schema defined here,
    and commiting the changes.
    """
    db.configure_mappers()
    db.create_all()
    db.session.commit()

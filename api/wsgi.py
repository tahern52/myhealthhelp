# This file tells gunicorn where to look for the API
from main import app as application

if __name__ == '__main__':
	application.run(threaded=True)

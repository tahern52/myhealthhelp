# MentalHealthHelp

Backend developed in Flask to provide a REST API to the frontend

To start the backend server from this directory:
```sudo nohup gunicorn wsgi -b 0.0.0.0:80```

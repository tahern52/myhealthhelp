"""
This holds the flask framework structure for the backend server
and defines the REST API
"""

from flask import Flask, render_template, jsonify, make_response
# from flask_io import FlaskIO, fields, Schema
from flask_sqlalchemy import SQLAlchemy
from flask_restless import APIManager
from flask_cors import CORS
from sqlalchemy_searchable import search
# from sqlalchemy import desc, create_engine
from models import Charity, Illness, Hospital
import pymysql


app = Flask(__name__)
CORS(app)
# io = FlaskIO(app)
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://mentalhelp:qwerqwer@rds-mysql-mentalhelp.cfgrep4u2t84.us-east-1.rds.amazonaws.com/mentalhelp"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 20
db = SQLAlchemy(app)

@app.route('/')
def hello():
	return "<h1>Welcome to the API. To get started, query the endpoints for charities, illnesses, and hospitals</h1>"

@app.errorhandler(404)
def page_not_found(e):
	return 'Error: this page does not exist', 404

"""
BEGIN CHARITIES API CALLS
"""
@app.route('/charities', methods=['GET'])
# @io.from_query('page', fields.Integer(missing=1))
# @io.from_query('entries_per_pg', fields.Integer(missing=10))
def get_charities(page=1, entries_per_pg=10, **kwargs):
	"""
	API GET request for all charities
	"""
	charities = [get_charity_data(c) for c in Charity.query.all()]
	return jsonify(charities)


@app.route('/charities/<int:id>', methods=['GET'])
def get_charity(id):
	"""
	API GET request for a single charity by its id
	"""
	charity = Charity.query.filter_by(id=id).first_or_404()
	return jsonify(get_charity_data(charity))

@app.route('/charities/size', methods=['GET'])
def get_num_charities():
	return '%d' % len(Charity.query.all())

def get_charity_data(charity):
	"""
	Create a dictionary to return attributes and values for a given charity
	"""
	return {
		'id':charity.id,
		'name':charity.name,
		'state':charity.state,
		'tagLine':charity.tagLine,
		'rating':charity.rating,
		'mission':charity.mission,
		'website_url':charity.website_url,
		'image_url':charity.image_url,
		'deductible':charity.deductible,
		'assetAmount':charity.assetAmount,
		'incomeAmount':charity.incomeAmount
	}


"""
BEGIN HOSPITALS API CALLS
"""
@app.route('/hospitals', methods=['GET'])
# @io.from_query('page', fields.Integer(missing=1))
# @io.from_query('entries_per_pg', fields.Integer(missing=10))
def get_hospitals(page=1, entries_per_pg=10, **kwargs):
	"""
	API GET request for all hospitals
	"""
	hospitals = [get_hospital_data(h) for h in Hospital.query.all()]
	return jsonify(hospitals)


@app.route('/hospitals/<int:id>', methods=['GET'])
def get_hospital(id):
	"""
	API GET request for a single hospital by its id
	"""
	hospital = Hospital.query.filter_by(id=id).first_or_404()
	return jsonify(get_hospital_data(hospital))

@app.route('/hospitals/size', methods=['GET'])
def get_num_hospitals():
	return '%d' % len(Hospital.query.all())

def get_hospital_data(hospital):
	"""
	Create a dictionary to return attributes and values for a given hospital
	"""
	return {
		'id':hospital.id,
		'name':hospital.name,
		'image_url':hospital.image_url,
		'owner':hospital.owner,
		'population':hospital.population,
		'website_url':hospital.website_url,
		'latitude':hospital.latitude,
		'longitude':hospital.longitude,
		'address':hospital.address,
		'state':hospital.state,
		'city':hospital.city,
		'zip_code':hospital.zip_code
	}


"""
BEGIN ILLNESSES API CALLS
"""
@app.route('/illnesses', methods=['GET'])
def get_illnesses(page=1, entries_per_pg=10, **kwargs):
	"""
	API GET request for all illnesses
	"""
	illnesses = [get_illness_data(i) for i in Illness.query.all()]
	return jsonify(illnesses)


@app.route('/illnesses/<int:id>', methods=['GET'])
def get_illness(id):
	"""
	API GET request for a single illness by its id
	"""
	illness = Illness.query.filter_by(id=id).first_or_404()
	return jsonify(get_illness_data(illness))

@app.route('/illnesses/size', methods=['GET'])
def get_num_illnesses():
	return '%d' % len(Illness.query.all())

def get_illness_data(illness):
	"""
	Create a dictionary to return attributes and values for a given illness
	"""
	return {
		'id':illness.id,
		'name':illness.name,
		'image_url':illness.image_url,
		'population':illness.population,
		'average_age':illness.average_age,
		'genetic':illness.genetic,
		'chronic':illness.chronic,
		'symptoms':illness.symptoms,
		'treatments':illness.treatments,
		'curable':illness.curable
	}

db.create_all()
db.session.commit()

# Create the Flask-Restless API manager.
manager = APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
manager.create_api(Illness, methods=['GET'], url_prefix='/', max_results_per_page=-1, results_per_page=-1)
manager.create_api(Hospital, methods=['GET'], url_prefix='/', max_results_per_page=-1, results_per_page=-1)
manager.create_api(Charity, methods=['GET'], url_prefix='/', max_results_per_page=-1, results_per_page=-1)

if __name__ == '__main__':

	# Run the flask app
	app.run(host='0.0.0.0', port=80, threaded=True)

"""
Unit tests for the mentalhelp.me API
"""

import requests, timeout_decorator
from unittest import TestCase, main

class APITest (TestCase):

	api_url = 'http://api.mentalhelp.me/'

	# def test_site(self):
	# 	"""
	# 	Test if the actual website itself is up and running - will fail for now
	# 	HTTP status code 200 means everything is ok
	# 	"""
	# 	self.assertEqual(requests.get('http://mentalhelp.me/').status_code, 200)

	def test_api_site(self):
		"""
		Test if the API itself is up and running
		HTTP status code 200 means everything is ok
		"""
		self.assertEqual(requests.get(self.api_url).status_code, 200)

	# Test API base tables
	def test_charities(self):
		"""
		Test if the API returns the charities
		"""
		charities = requests.get(self.api_url + 'Charities').json()
		names = [
		'American Foundation for Suicide Prevention', 
		'NAMI', 
		'Mental Health America',
		'Brain & Behavior Research Foundation', 
		'Mental Health Association', 
		'Mental Health Colorado',
		'Mental Health Association in Tulsa', 
		'Depression and Bipolar Support Alliance, National Office',
		'The Jed Foundation', 
		'Mental Health Advocacy Services'
		]
		self.assertEqual(charities['num_results'], len(charities['objects']))
		self.assertTrue(all(x in (i['name'] for i in charities['objects']) for x in names))

	def test_hospitals(self):
		"""
		Test if the API returns the hospitals
		"""
		hospitals = requests.get(self.api_url + 'Hospitals').json()
		names = [
		'ROLLING HILLS HOSPITAL', 
		'CUMBERLAND HALL HOSPITAL', 
		'RIVER VALLEY BEHAVIORAL HEALTH',
		'AUSTIN STATE HOSPITAL', 
		'HARRIS COUNTY PSYCHIATRIC CENTER', 
		'THE MENNINGER CLINIC',
		'CROSS CREEK HOSPITAL', 
		'RESNICK NEUROPSYCHIATRIC HOSPITAL AT UCLA',
		'MCLEAN HOSPITAL CORPORATION', 
		'SHEPPARD AND ENOCH PRATT HOSPITAL'
		]
		self.assertEqual(hospitals['num_results'], len(hospitals['objects']))
		self.assertTrue(all(x in (i['name'] for i in hospitals['objects']) for x in names))

	def test_illnesses(self):
		"""
		Test if the API returns the illnesses
		"""
		illnesses = requests.get(self.api_url + 'Illnesses').json()
		names = [
		'ADHD', 
		'Schizophrenia', 
		'Anxiety Disorder',
		'Bipolar Disorder', 
		'Borderline Personality Disorder', 
		'Post-Traumatic Stress Disorder',
		'Dissociative Identity Disorder', 
		'Obsessive-Compulsive Disorder',
		'Narcolepsy', 
		'Depression'
		]
		self.assertEqual(illnesses['num_results'], len(illnesses['objects']))
		self.assertTrue(all(x in (i['name'] for i in illnesses['objects']) for x in names))

	def test_ascending(self):
		"""
		Test if API requests are returned in ascending order 
		by ID with optional direction parameter
		"""
		params = '{ \
			"order_by":[{ \
				"field":"id", \
				"direction":"asc" \
			}] \
		}'
		charities = requests.get(self.api_url + 'Charities?q=' + params).json()['objects']
		hospitals = requests.get(self.api_url + 'Hospitals?q=' + params).json()['objects']
		illnesses = requests.get(self.api_url + 'Illnesses?q=' + params).json()['objects']

		self.assertTrue(all(x['id'] < y['id'] for x, y in zip(charities, charities[1:])))
		self.assertTrue(all(x['id'] < y['id'] for x, y in zip(hospitals, hospitals[1:])))
		self.assertTrue(all(x['id'] < y['id'] for x, y in zip(illnesses, illnesses[1:])))

	"""
	Individual API tests
	"""

	# Test Charities
	def test_charity_by_id(self):
		charity = requests.get(self.api_url + 'Charities/10').json()
		self.assertEqual(charity['name'], 'Brain & Behavior Research Foundation')

	# def test_charity_connections(self):
	# 	"""
	# 	Ensure the related hospitals and illnesses are contained within
	# 	"""
	# 	charities = requests.get(self.api_url + 'Charities').json()
	# 	self.assertTrue(? in charities['hospital_ids'])
	# 	self.assertTrue(? in charities['illness_ids'])

	# Test Hospitals
	def test_hospital_by_id(self):
		hospital = requests.get(self.api_url + 'Hospitals/12').json()
		self.assertEqual(hospital['name'], 'ROLLING HILLS HOSPITAL')

	# def test_hospital_connections(self):
	# 	"""
	# 	Ensure the related charities and illnesses are contained within
	# 	"""
	# 	hospitals = requests.get(self.api_url + 'Hospitals').json()
	# 	self.assertTrue(? in hospitals['charity_ids'])
	# 	self.assertTrue(? in hospitals['illness_ids'])

	# Test Illnesses
	def test_illness_by_id(self):
		illness = requests.get(self.api_url + 'Illnesses/14').json()
		self.assertEqual(illness['name'], 'Anxiety Disorder')

	# def test_illness_connections(self):
	# 	"""
	# 	Ensure the related charities and hospitals are contained within
	# 	"""
	# 	illnesses = requests.get(self.api_url + 'Illnesses').json()
	# 	self.assertTrue(? in illnesses['charity_ids'])
	# 	self.assertTrue(? in illnesses['hospital_ids'])


if __name__ == '__main__':
	timeout_decorator.timeout(1000)(main)()
